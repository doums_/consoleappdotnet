﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using Newtonsoft.Json;

namespace Server
{
    public class Round
    {
        private enum State
        {
            WaitForPlayer,
            Next,
            Finished,
            Exit
        };
        
        private State RState { get; set; }
        private List<Player> Players { get; set; }
        private Heap Heap { get; set; }
        private Player LastPlayer { get; set; }
        private int NbrPass { get; set; }
        
        public Round(List<Player> players)
        {
            Heap = new Heap();
            Players = players;
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Shot", ShotHandler);
            NetworkComms.AppendGlobalIncomingPacketHandler<int>("Pass", PassHandler);
            RState = State.Next;
            NbrPass = 0;
        }

        public int Run(int i)
        {
            Console.WriteLine("New round.");
            while (true)
            {
                if (RState == State.Next)
                {
                    NextPlayer(i);
                    i++;
                    if (i == Players.Count)
                        i = 0; 
                }
                if (RState == State.Exit)
                    return 0;
                if (RState == State.Finished)
                    return WinHandler();
                if (LastPlayer != null && NbrPass >= Players.Count)
                    return WinHandler();
                Thread.Sleep(50);
            }
        }

        private void NextPlayer(int i)
        {
            if (RState == State.Finished || (LastPlayer != null && NbrPass >= Players.Count))
                return;
            Console.WriteLine(Players[i].Name + "'s turn");
            var broadcast = new Broadcast("It's " + Players[i].Name + "'s turn");
            broadcast.PlayerId.Add(Players[i].Id);
            Broadcast("Ad", broadcast);
            var jsonHeap = JsonConvert.SerializeObject(Heap);
            Players[i].Connection.SendObject("YourTurn", jsonHeap);
            RState = State.WaitForPlayer;
        }
        
        private int WinHandler()
        {
            Console.WriteLine(LastPlayer.Name + " wins the round!");
            var broadcast = new Broadcast(LastPlayer.Name + " wins the round!");
            Broadcast("Ad", broadcast);
            return Players.IndexOf(LastPlayer);
        }

        private void ShotHandler(PacketHeader header, Connection connection, string jsonShot)
        {
            if (RState != State.WaitForPlayer)
                return;
            var shot = JsonConvert.DeserializeObject<Shot>(jsonShot);
            var player = FindPlayerById(shot.PlayerId);
            Heap.CardNumber = shot.Cards.Count;
            Heap.Value = shot.Cards.First().CValue;
            var msg = player.Name + " plays";
            foreach (var card in shot.Cards)
                msg += "\n" + card;
            Console.WriteLine(msg);
            var broadcast = new Broadcast(msg);
            broadcast.PlayerId.Add(shot.PlayerId);
            Broadcast("Ad", broadcast);
            LastPlayer = player;
            NbrPass = 0;
            RState = Heap.Value == Card.Value.Deux ? State.Finished : State.Next;
        }

        private void PassHandler(PacketHeader header, Connection connection, int id)
        {
            if (RState != State.WaitForPlayer)
                return;
            var player = FindPlayerById(id);
            Console.WriteLine(player.Name + " pass");
            var broadcast = new Broadcast(player.Name + " pass");
            broadcast.PlayerId.Add(id);
            Broadcast("Ad", broadcast);
            NbrPass++;
            RState = State.Next;
        }

        private void Broadcast(string type, Broadcast broadcast)
        {
            foreach (var player in Players)
            {
                if (!broadcast.PlayerId.Contains(player.Id))
                    player.Connection.SendObject(type, broadcast.Message);
            }
        }
        
        private Player FindPlayerById(int id)
        {
            return Players.FirstOrDefault(player => player.Id == id);
        }
    }
}