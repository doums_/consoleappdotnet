﻿using System;
using System.Collections.Generic;

namespace Server
{      
    public class Card
    {
        public enum Color
        {
            Pique = 0,
            Coeur,
            Carreau,
            Trèfle
        }

        public enum Value
        {
            Trois = 0,
            Quatre = 1,
            Cinq = 2,
            Six = 3,
            Sept = 4,
            Huit = 5,
            Neuf = 6,
            Dix = 7,
            Valet = 8,
            Dame = 9,
            Roi = 10,
            As = 11,
            Deux = 12,
            Undefined
        }
        
        public Color CColor { get; set; }
        public Value CValue { get; set; }
        public int Id { get; set; }
        private static int _count;

        public Card(Color cColor, Value cValue)
        {
            CColor = cColor;
            CValue = cValue;
            Id = _count;
            _count++;
        }
        
        public Card() {}
        
        public override string ToString()
        {
            string color = "";
            string value = "";

            switch (CColor)
            {
                case Color.Carreau:
                    color = "Carreau";
                    break;
                case Color.Coeur:
                    color = "Coeur";
                    break;
                case Color.Pique:
                    color = "Pique";
                    break;
                case Color.Trèfle:
                    color = "Trèfle";
                    break;
                            
            }
            switch (CValue)
            {
                case Value.As:
                    value = "As";
                    break;
                case Value.Deux:
                    value = "Deux";
                    break;
                case Value.Trois:
                    value = "Trois";
                    break;
                case Value.Quatre:
                    value = "Quatre";
                    break;
                case Value.Cinq:
                    value = "Cinq";
                    break;
                case Value.Six:
                    value = "Six";
                    break;
                case Value.Sept:
                    value = "Sept";
                    break;
                case Value.Huit:
                    value = "Huit";
                    break;
                case Value.Neuf:
                    value = "Neuf";
                    break;
                case Value.Dix:
                    value = "Dix";
                    break;
                case Value.Valet:
                    value = "Valet";
                    break;
                case Value.Dame:
                    value = "Dame";
                    break;
                case Value.Roi:
                    value = "Roi";
                    break;        
            }
            return value + "-" + color;
        }
    }
    
    public class CardGame
    {
        public List<Card> Cards { get; set; }
        private Random Rand { get; set; }
    
        public CardGame()
        {
            Rand = new Random();
            Cards = new List<Card>
            {
                new Card(Card.Color.Pique, Card.Value.As),
                new Card(Card.Color.Pique, Card.Value.Deux),
                new Card(Card.Color.Pique, Card.Value.Trois),
                new Card(Card.Color.Pique, Card.Value.Quatre),
                new Card(Card.Color.Pique, Card.Value.Cinq),
                new Card(Card.Color.Pique, Card.Value.Six),
                new Card(Card.Color.Pique, Card.Value.Sept),
                new Card(Card.Color.Pique, Card.Value.Huit),
                new Card(Card.Color.Pique, Card.Value.Neuf),
                new Card(Card.Color.Pique, Card.Value.Dix),
                new Card(Card.Color.Pique, Card.Value.Valet),
                new Card(Card.Color.Pique, Card.Value.Dame),
                new Card(Card.Color.Pique, Card.Value.Roi),
                new Card(Card.Color.Carreau, Card.Value.As),
                new Card(Card.Color.Carreau, Card.Value.Deux),
                new Card(Card.Color.Carreau, Card.Value.Trois),
                new Card(Card.Color.Carreau, Card.Value.Quatre),
                new Card(Card.Color.Carreau, Card.Value.Cinq),
                new Card(Card.Color.Carreau, Card.Value.Six),
                new Card(Card.Color.Carreau, Card.Value.Sept),
                new Card(Card.Color.Carreau, Card.Value.Huit),
                new Card(Card.Color.Carreau, Card.Value.Neuf),
                new Card(Card.Color.Carreau, Card.Value.Dix),
                new Card(Card.Color.Carreau, Card.Value.Valet),
                new Card(Card.Color.Carreau, Card.Value.Dame),
                new Card(Card.Color.Carreau, Card.Value.Roi),
                new Card(Card.Color.Coeur, Card.Value.As),
                new Card(Card.Color.Coeur, Card.Value.Deux),
                new Card(Card.Color.Coeur, Card.Value.Trois),
                new Card(Card.Color.Coeur, Card.Value.Quatre),
                new Card(Card.Color.Coeur, Card.Value.Cinq),
                new Card(Card.Color.Coeur, Card.Value.Six),
                new Card(Card.Color.Coeur, Card.Value.Sept),
                new Card(Card.Color.Coeur, Card.Value.Huit),
                new Card(Card.Color.Coeur, Card.Value.Neuf),
                new Card(Card.Color.Coeur, Card.Value.Dix),
                new Card(Card.Color.Coeur, Card.Value.Valet),
                new Card(Card.Color.Coeur, Card.Value.Dame),
                new Card(Card.Color.Coeur, Card.Value.Roi),
                new Card(Card.Color.Trèfle, Card.Value.As),
                new Card(Card.Color.Trèfle, Card.Value.Deux),
                new Card(Card.Color.Trèfle, Card.Value.Trois),
                new Card(Card.Color.Trèfle, Card.Value.Quatre),
                new Card(Card.Color.Trèfle, Card.Value.Cinq),
                new Card(Card.Color.Trèfle, Card.Value.Six),
                new Card(Card.Color.Trèfle, Card.Value.Sept),
                new Card(Card.Color.Trèfle, Card.Value.Huit),
                new Card(Card.Color.Trèfle, Card.Value.Neuf),
                new Card(Card.Color.Trèfle, Card.Value.Dix),
                new Card(Card.Color.Trèfle, Card.Value.Valet),
                new Card(Card.Color.Trèfle, Card.Value.Dame),
                new Card(Card.Color.Trèfle, Card.Value.Roi)
            };
        }
        
        public void Shuffle()
        {
            int n = Cards.Count;
            while (n > 1)
            {
                n--;
                int k = Rand.Next(n + 1);
                Card value = Cards[k];
                Cards[k] = Cards[n];
                Cards[n] = value;
            }
        }
    }
}