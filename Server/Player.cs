﻿using NetworkCommsDotNet.Connections;

namespace Server
{
    public class Player
    {
        public enum State
        {
            NoName,
            LobbyReady,
            LobbyNotReady
        }

        public State PState { get; set; }
        private static int _count;
        public int Id { get; set; }
        public string Name { get; private set; }
        public string Ip { get; set; }
        public int Port { get; set; }
        public Connection Connection { get; set; }
        public bool ReadyToReceive { get; set; }

        public Player(string name)
        {
            Id = _count;
            _count++;
            Name = name;
            PState = State.NoName;
            ReadyToReceive = true;
        }
    }
}