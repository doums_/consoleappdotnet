﻿using System.Collections.Generic;

namespace Server
{
    public class Shot
    {
        public List<Card> Cards { get; set; }
        public int PlayerId { get; set; }
        
        public Shot(int playerId)
        {
            PlayerId = playerId;
            Cards = new List<Card>();
        }
    }
}