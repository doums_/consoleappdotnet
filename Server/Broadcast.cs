﻿using System.Collections.Generic;

namespace Server
{
    public class Broadcast
    {
        public List<int> PlayerId { get; set; }
        public string Message { get; set; }

        public Broadcast(string message)
        {
            PlayerId = new List<int>();
            Message = message;
        }
    }
}