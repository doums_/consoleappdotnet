﻿using System;

namespace Server
{
    public class Heap
    {
        public Card.Value Value { get; set; }
        public int CardNumber { get; set; }

        public Heap()
        {
            Value = Card.Value.Undefined;
            CardNumber = -1;
        }
        
        public override string ToString()
        {
            var value = "";
            switch (Value)
            {
                case Card.Value.Trois:
                    value = "Trois";
                    break;
                case Card.Value.Quatre:
                    value = "Quatre";
                    break;
                case Card.Value.Cinq:
                    value = "Cinq";
                    break;
                case Card.Value.Six:
                    value = "Six";
                    break;
                case Card.Value.Sept:
                    value = "Sept";
                    break;
                case Card.Value.Huit:
                    value = "Huit";
                    break;
                case Card.Value.Neuf:
                    value = "Neuf";
                    break;
                case Card.Value.Dix:
                    value = "Dix";
                    break;
                case Card.Value.Valet:
                    value = "Valet";
                    break;
                case Card.Value.Dame:
                    value = "Dame";
                    break;
                case Card.Value.Roi:
                    value = "Roi";
                    break;
                case Card.Value.As:
                    value = "As";
                    break;
                case Card.Value.Deux:
                    value = "Deux";
                    break;
                case Card.Value.Undefined:
                    value = "Undefined";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return "Heap value " + value + " card number " + CardNumber;
        }
    }
}