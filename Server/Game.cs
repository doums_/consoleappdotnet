﻿using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet;
using System.Net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;

namespace Server
{
    public class Game
    {
        private enum State
        {
            Finished,
            Run,
            Lobby
        };

        private State GState { get; set; }
        private List<Player> Players { get; set; }
        private CardGame CardGame { get; set; }
        private Random Rand { get; set; }

        public Game()
        {
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Name", GetClientName);
            NetworkComms.AppendGlobalIncomingPacketHandler<int>("ReadyToPlay", ReadyToPlay);
            NetworkComms.AppendGlobalIncomingPacketHandler<int>("ReadyToReceive", ReadyToReceive);
            NetworkComms.AppendGlobalIncomingPacketHandler<int>("Win", WinHandler);
            NetworkComms.AppendGlobalConnectionEstablishHandler(ClientConnected);
            NetworkComms.AppendGlobalConnectionCloseHandler(ClientDisconnected);

            Connection.StartListening(ConnectionType.UDP, new IPEndPoint(IPAddress.Any, 0));
            Console.WriteLine("Server listening for UDP connection on:");
            foreach (System.Net.IPEndPoint localEndPoint in Connection.ExistingLocalListenEndPoints(ConnectionType.UDP))
                Console.WriteLine("{0}:{1}", localEndPoint.Address, localEndPoint.Port);
            Players = new List<Player>();
            GState = State.Lobby;
            Rand = new Random();
        }

        public void Lobby()
        {
            while (true)
            {
                Thread.Sleep(500);
                if (GState == State.Lobby && Players.Count >= 2 && AllReady())
                {
                    GState = State.Run;
                    break;
                }
            }
        }

        public void Run()
        {
            CardGame = new CardGame();
            CardGame.Shuffle();
            Console.WriteLine("Game Start");
            var broadcast = new Broadcast("Game Start");
            Broadcast("Ad", broadcast);
            ToDistribute();
            var firstRound = true;
            var i = 0;
            while (true)
            {
                var round = new Round(Players);
                if (GState == State.Run && firstRound)
                {
                    i = round.Run(Rand.Next(Players.Count));
                    firstRound = false;
                }
                else if (GState == State.Finished)
                    return;
                else
                    i = round.Run(i);
            }
        }
        
        private void ClientConnected(Connection client)
        {
            Console.WriteLine("New client connected - " + client.ConnectionInfo.RemoteEndPoint);
            client.SendObject(Players.Count > 5 || GState == State.Run ? "NoRoom" : "Name");
        }

        private static void ClientDisconnected(Connection client)
        {
            Console.WriteLine("A client has disconnected - " + client);
        }

        private void GetClientName(PacketHeader header, Connection connection, string name)
        {
            Console.WriteLine("The new client is " + name);

            var player = new Player(name);
            player.PState = Player.State.LobbyNotReady;
            var clientAdress = connection.ConnectionInfo.RemoteEndPoint.ToString();
            player.Ip = clientAdress.Split(':').First();
            player.Port = int.Parse(clientAdress.Split(':').Last());
            player.Connection = connection;
            Players.Add(player);
            connection.SendObject("Id", player.Id);
            player.ReadyToReceive = false;
            while (!player.ReadyToReceive) {}
            connection.SendObject("GoToLobby");
        }

        private void ReadyToPlay(PacketHeader header, Connection connection, int id)
        {
            foreach (var player in Players)
            {
                if (player.Id != id) continue;
                player.PState = Player.State.LobbyReady;
                Console.WriteLine(player.Name + " is ready.");
                break;
            }
        }
        
        private bool AllReady()
        {
            return Players.All(player => player.PState == Player.State.LobbyReady);
        }

        private void ToDistribute()
        {
            var i = CardGame.Cards.Count / Players.Count;
            i = i * Players.Count;

            var cardStack = new Stack();
            foreach (var card in CardGame.Cards)
                cardStack.Push(card);

            while (i > 0)
            {
                foreach (var player in Players)
                {
                    var card = cardStack.Pop();
                    var jsonCard = JsonConvert.SerializeObject(card);
                    while (!player.ReadyToReceive) {}
                    player.ReadyToReceive = false;
                    player.Connection.SendObject("Card", jsonCard);
                    i--;
                }
            }
        }

        private void Broadcast(string type, Broadcast broadcast)
        {
            foreach (var player in Players)
            {
                if (!broadcast.PlayerId.Contains(player.Id))
                    player.Connection.SendObject(type, broadcast.Message);
            }
        }

        private void ReadyToReceive(PacketHeader header, Connection connection, int id)
        {
            FindPlayerById(id).ReadyToReceive = true;
        }

        private Player FindPlayerById(int id)
        {
            return Players.FirstOrDefault(player => player.Id == id);
        }

        private void WinHandler(PacketHeader header, Connection connection, int id)
        {
            Console.WriteLine(FindPlayerById(id).Name + " win the game!!!");
            var msg = new Broadcast(FindPlayerById(id).Name + " win the game!!!");
            msg.PlayerId.Add(id);
            var broadcast = new Broadcast("GameWin");
            Broadcast("GameWin", broadcast);
            GState = State.Finished;
        }
    }
}