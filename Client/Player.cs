﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.UDP;
using Newtonsoft.Json;

namespace Client
{
    public class Player
    {
        private enum State
        {
            Lobby,
            InGame,
            Exit
        }
        
        private State PState { get; set; }
        private string Name { get; set; }
        private string Ip { get; set; }
        private int Port { get; set; }
        private string ServerIp { get; set; }
        private int ServerPort { get; set; }
        private Connection ClientConnection { get; set; }
        private int Id { get; set; }
        private bool LobbyReady { get; set; }
        public List<Card> Hand { get; set; }

        public Player(string serverIp, int serverPort)
        {
            ServerIp = serverIp;
            ServerPort = serverPort;
            LobbyReady = false;
            Hand = new List<Card>();
            PState = State.Lobby;
        }

        public void Connect()
        {
            ConnectionInfo connInfo = new ConnectionInfo(ServerIp, ServerPort);
            
            ClientConnection = UDPConnection.GetConnection(connInfo, UDPOptions.None);
            ClientConnection.AppendIncomingPacketHandler<string>("Name", ReplyName);
            ClientConnection.AppendIncomingPacketHandler<int>("Id", GetId);
            ClientConnection.AppendIncomingPacketHandler<string>("GoToLobby", GoToLobby);
            ClientConnection.AppendIncomingPacketHandler<string>("Card", ReceivedCard);
            ClientConnection.AppendIncomingPacketHandler<string>("GameStart", NewRound);
            ClientConnection.AppendIncomingPacketHandler<string>("YourTurn", YourTurn);
            ClientConnection.AppendIncomingPacketHandler<string>("Ad", Ad);
            ClientConnection.AppendIncomingPacketHandler<string>("GameWin", GameWin);
            ClientConnection.AppendIncomingPacketHandler<string>("NoRoom", NoRoom);

            string clientAdress = NetworkComms.GetExistingConnection(connInfo).ConnectionInfo.LocalEndPoint.ToString();
            Ip = clientAdress.Split(':').First();
            Port = int.Parse(clientAdress.Split(':').Last());
            
            ClientConnection.SendObject("Hello Server");
            while (true)
            {
                if (PState == State.Exit)
                    return;
                Thread.Sleep(100);
            }
        }

        private void ReplyName(PacketHeader header, Connection connection, string message)
        {
            while (true)
            {
                Console.WriteLine("Please enter your name");
                string name = Console.ReadLine();
                if (string.IsNullOrEmpty(name))
                {
                    Console.WriteLine("Enter a valid name");
                    continue;
                }
                Name = name;
                ClientConnection.SendObject("Name", Name);
                break;
            }
        }

        private void GetId(PacketHeader header, Connection connection, int id)
        {
            Id = id;
            ClientConnection.SendObject("ReadyToReceive", Id);
        }

        private void GoToLobby(PacketHeader header, Connection connection, string message)
        {
            Lobby();
        }

        private void Lobby()
        {
            Console.WriteLine("Enter in Lobby, wait for players.");
            Console.WriteLine("Press Y when you are ready.");
            while (true)
            {
                if (Console.ReadKey(true).Key == ConsoleKey.Y)
                {
                    ClientConnection.SendObject("ReadyToPlay", Id);
                    LobbyReady = true;
                    Console.WriteLine("Ready");
                    break;
                }
            }
            Round();
        }

        private void ReceivedCard(PacketHeader header, Connection connection, string jsonCard)
        {
            var card = JsonConvert.DeserializeObject<Card>(jsonCard);
            Hand.Add(card);
            ClientConnection.SendObject("ReadyToReceive", Id);
            Console.WriteLine("You receive " + card);
        }

        private void NewRound(PacketHeader header, Connection connection, string message)
        {
            PState = State.InGame;
            Console.WriteLine("Game Start");
        }
        
        private void Round()
        {
            while (true)
            {
                if (PState == State.Exit)
                    return;
                Thread.Sleep(100);
            }
        }

        private void YourTurn(PacketHeader header, Connection connection, string jsonHeap)
        {
            Console.WriteLine("It's your turn my friend");
            var heap = JsonConvert.DeserializeObject<Heap>(jsonHeap);
            if (heap.Value != Card.Value.Undefined)
                Console.WriteLine(heap);
            Console.WriteLine("Your hand");
            foreach (var card in Hand)
                Console.WriteLine(card.Id + " " + card);
            if (heap.Value != Card.Value.Undefined && !CheckIfPossible(heap))
            {
                ClientConnection.SendObject("Pass", Id);
                Console.WriteLine("You pass");
                return;
            }
            Console.WriteLine("Press Y to select your card(s) or N to pass your turn");
            bool pass;
            while (true)
            {
                var key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.Y)
                {
                    pass = false;
                    break;
                }
                if (key != ConsoleKey.N) continue;
                pass = true;
                break;
            }
            if (pass)
            {
                ClientConnection.SendObject("Pass", Id);
                Console.WriteLine("You pass");
            }
            else
            {
                Shot shot;
                while (true)
                {
                    Console.WriteLine("Select your cards (e.g. CardId-CardId-CardId)");
                    string choice = Console.ReadLine();
                    if (string.IsNullOrEmpty(choice) || !CheckShot(choice, heap, out shot))
                    {
                        Console.WriteLine("Enter a valid cards choice");
                        continue;
                    }
                    break;
                }
                Console.WriteLine("You play");
                foreach (var card in shot.Cards)
                    Console.WriteLine(card);
                ClientConnection.SendObject("Shot", JsonConvert.SerializeObject(shot));
                RefreshHand(shot);
            }
        }

        private bool CheckIfPossible(Heap heap)
        {
            foreach (var card in Hand)
            {
                if (card.CValue >= heap.Value && CheckNumber(card.CValue) >= heap.CardNumber)
                    return true;
            }
            return false;
        }
        
        private int CheckNumber(Card.Value value)
        {
            var i = 0;
            foreach (var card in Hand)
            {
                if (card.CValue == value)
                    i++;
            }
            return i;
        }
        
        private bool CheckShot(string choice, Heap heap, out Shot shot)
        {
            shot = new Shot(Id);
            var split = choice.Split('-');
            foreach (var s in split)
            {
                if (s.Trim() != "")
                {
                    int j;
                    Card card;
                    if (int.TryParse(s, out j) && RetrievedCard(j, out card) && !AlreadyIn(shot, card))
                        shot.Cards.Add(card);
                    else
                        return false;
                }
                else
                    return false;
            }
            var i = shot.Cards.First().CValue;
            foreach (var c in shot.Cards)
            {
                if (c.CValue != i)
                    return false;
            }
            if (heap.Value != Card.Value.Undefined && i < (Card.Value) heap.Value)
                return false;
            if (heap.Value != Card.Value.Undefined && shot.Cards.Count != heap.CardNumber)
                return false;
            return true;
        }

        private bool RetrievedCard(int id, out Card card)
        {
            card = new Card();
            foreach (var c in Hand)
            {
                if (c.Id != id) continue;
                card = c;
                return true;
            }
            return false;
        }

        private bool AlreadyIn(Shot shot, Card card)
        {
            return shot.Cards.Any(c => c == card);
        }

        private void RefreshHand(Shot shot)
        {
            foreach (var card in shot.Cards)
            {
                if (Hand.Find(handCard => handCard == card) != null)
                    Hand.Remove(card);

            }
            if (!Hand.Any())
            {
                ClientConnection.SendObject("Win", Id);
                Console.WriteLine("You WIN the game!!!");
                PState = State.Exit;
            }
        }

        private void Ad(PacketHeader header, Connection connection, string ad)
        {
            Console.WriteLine(ad);
        }
        
        private void GameWin(PacketHeader header, Connection connection, string ad)
        {
            Console.WriteLine(ad);
            Console.WriteLine("You lose, you bad");
            PState = State.Exit;
        }
        
        private void NoRoom(PacketHeader header, Connection connection, string ad)
        {
            Console.WriteLine("No more room");
            PState = State.Exit;
        }
    }
}