﻿using System;
using System.Linq;
using NetworkCommsDotNet;

namespace Client
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter the server IP:PORT");
            string serverInfo = Console.ReadLine();

            string serverIp;
            int serverPort;
            try
            {
                serverIp = serverInfo.Split(':').First();
                serverPort = int.Parse(serverInfo.Split(':').Last());
            }
            catch ( System.Exception e)
            {
                Console.WriteLine("Bad IP:PORT " + e.Message);
                return;
            }
            Player player = new Player(serverIp, serverPort);
            try
            {
                player.Connect();
            }
            catch (CommsException e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }
            catch (System.ArgumentException e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }
            NetworkComms.Shutdown();
        }
    }
}