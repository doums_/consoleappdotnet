﻿using System.Collections.Generic;

namespace Client
{
    public class Shot
    {
        public List<Card> Cards { get; set; }
        public int PlayerId { get; set; }
        
        public Shot(int playerId)
        {
            PlayerId = playerId;
            Cards = new List<Card>();
        }
    }
}